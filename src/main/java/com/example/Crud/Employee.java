package com.example.Crud;

public class Employee {
private int id;
private String name;
private String email;
private String gender;
private String dept;
private double salary;

public Employee() {
	super();
}

public Employee(int id, String name, String email, String gender, String dept, double salary) {
	super();
	this.id = id;
	this.name = name;
	this.email = email;
	this.gender = gender;
	this.dept = dept;
	this.salary = salary;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getDept() {
	return dept;
}

public void setDept(String dept) {
	this.dept = dept;
}

public double getSalary() {
	return salary;
}

public void setSalary(double salary) {
	this.salary = salary;
}




}
